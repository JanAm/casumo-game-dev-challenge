# Casumo Game Dev Challenge

My application for the Casumo game dev challenge

## Game concept 

I was thinking about a few ideas which use the dynamic of the wheel spin but are not a simple wheel. I came up with this idea about pirate ships which are sailing in a circle. I did make them spin unrealistically fast because spinning at a high speed was mentioned in the task.

## Code concept

I started this task as I would start a real game. First I setup `webpack` and general folder structure and then I proceed to create a very simple entity-component system. I am a big fan of that pattern and I usually model it roughly on one of the bigger frameworks (such as [AFrame](https://aframe.io/docs/0.8.0/introduction/entity-component-system.html)) so it is clearer and easier for other developers to start working on it.

Next area of focus were actual components, I've created few of them to really show the power of this simple system and how we can move all the game data and parameters to different files which I call `scene definition`. I also tried to make it so, that the components are interchangeable as much as possible.

## Code Features 
 - simple entity component-system which allows us to do many cool things
 - structured and well documented code
 - few unit-tests to show how mocking canvas works and how we could test the logical parts of the game
 - wepback to create a bundle or a production build (in a real case this is where the babel transpilation would also happen)

## Game Features 
 - reel spin with winning lines 
 - basic balance bar where winnings are also reflected
 - few example sounds
 - game is responsive and it keeps 16:9 ratio - but we can see more details on wider screens

## Installing

Installation is very simple and there is only one thing that can be problematic. That is the `mock-canvas` module which has to be partly manually installed on some systems (for example Windows). If you have any issues please just remove  line `"canvas": "1.6.13"` from package.json and everything should work fine except you won't be able to run tests.

Steps to install:

```
npm i
```
## Running
```
npm run start:dev
```
## Generating documentation
```
npm run docs
```
## Running tests
```
npm run test
```

## Creating a production version
```
npm run build
```

## Built With

* [Webpack](https://webpack.js.org/) - Code and assets bundler
* [PixiJS](http://www.pixijs.com/) - The rendering engine used
* [HowlerJS](https://howlerjs.com/) - Audio library used
* [tween.js](https://github.com/tweenjs/tween.js/) - Very simple tween system
* [AVA](https://github.com/avajs/ava) - Test runner used

## Credits:
Audio: 
 - <https://opengameart.org/content/battle-theme-a>

Art:
 - <https://www.kenney.nl/>

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


