import ava from 'ava';
import * as PIXI from 'pixi.js';
import SceneManager from '../../../src/sceneManager/sceneManager';
import Wheel from '../../../src/components/slot/wheel';
import Button from '../../../src/components/button';
import Sprite from '../../../src/components/sprite';
import MovingObject from '../../../src/components/movingObject';
import Bar from '../../../src/components/bar';

global.sceneManager = null;
let components = null;

ava.before(() => {
  PIXI.utils.TextureCache = {
    spinButton: PIXI.Texture.EMPTY,
    ship0: PIXI.Texture.EMPTY,
    island: PIXI.Texture.EMPTY,
    ship1: PIXI.Texture.EMPTY,
    ship2: PIXI.Texture.EMPTY,
    ship3: PIXI.Texture.EMPTY,
    ship4: PIXI.Texture.EMPTY,
    ship5: PIXI.Texture.EMPTY,
    reelBackground: PIXI.Texture.EMPTY,
    canonIsland: PIXI.Texture.EMPTY,
    cloud: PIXI.Texture.EMPTY,
    textBackground: PIXI.Texture.EMPTY,
  };

  components = {
    Wheel,
    Button,
    Sprite,
    MovingObject,
    Text,
    Bar,
  };
  sceneManager = new SceneManager();
});

ava('should create a new sceneManager', (t) => {
  sceneManager.init(new PIXI.Application(), components);
  t.true(sceneManager.pixiApp instanceof PIXI.Application);
});
