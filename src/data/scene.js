import * as bar from './bar';
import * as slot from './slot';

export default
{
  id: 'mainScene',
  attributes: {
    maxWidth: 1280,
    maxHeight: 720,
  },
  entities: [
    {
      id: 'reel-background',
      display: {
        position: {
          x: 260,
          y: 20,
        },
        scale: {
          x: 0.9,
          y: 1.02,
        },
        alpha: 0,
      },
      attributes: {
        textureId: 'reelBackground',
      },
      components: [
        'Sprite',
      ],
    },
    slot.default,
    {
      id: 'spin-button',
      display: {
        position: {
          x: 1200,
          y: 360,
        },
      },
      attributes: {
        touchEvent: 'spin',
        textureId: 'spinButton',
      },
      entities: [
      ],
      components: [
        'Button',
      ],
    },
    {
      id: 'cloud',
      display: {
        position: {
          x: -200,
          y: 200,
        },
        scale: {
          x: 0.7,
          y: 0.7,
        },
        alpha: 0.7,
      },
      attributes: {
        textureId: 'cloud',
        duration: 30000,
      },
      components: [
        'Sprite', 'MovingObject',
      ],
      entities: [
        {
          id: 'small-cloud',
          display: {
            position: {
              x: 0,
              y: 30,
            },
            scale: {
              x: 0.7,
              y: 0.7,
            },
            alpha: 0.6,
          },
          attributes: {
            textureId: 'cloud',
          },
          components: [
            'Sprite',
          ],
        },
      ],
    },
    {
      id: 'cloud2',
      display: {
        position: {
          x: -300,
          y: 600,
        },
        scale: {
          x: 0.4,
          y: 0.4,
        },
        alpha: 0.6,
      },
      attributes: {
        textureId: 'cloud',
        duration: 25000,
      },
      components: [
        'Sprite', 'MovingObject',
      ],
    },
    bar.default,
  ],
};
