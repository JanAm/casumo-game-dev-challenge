import TWEEN from '@tweenjs/tween.js';

export default
{
  id: 'slot',
  display: {
    position: {
      x: 640,
      y: 355,
    },
    scale: {
      x: 0.85,
      y: 0.85,
    },
  },
  entities: [
    {
      id: 'top-left-canon-island',
      display: {
        position: {
          x: -800,
          y: -400,
        },
        scale: {
          x: 0.7,
          y: 0.7,
        },
        alpha: 1,
      },
      attributes: {
        textureId: 'canonIsland',
      },
      components: [
        'Sprite',
      ],
    },
    {
      id: 'bottom-left-canon-island',
      display: {
        position: {
          x: -800,
          y: 200,
        },
        scale: {
          x: 0.4,
          y: 0.4,
        },
        alpha: 1,
      },
      attributes: {
        textureId: 'canonIsland',
      },
      components: [
        'Sprite',
      ],
    },
    {
      id: 'middle-island',
      display: {
        position: {
          x: -100,
          y: -100,
        },
        scale: {
          x: 0.4,
          y: 0.4,
        },
      },
      attributes: {
        textureId: 'island',
      },
      components: [
        'Sprite',
      ],
    },
  ],
  components: [
    'Wheel',
  ],
  attributes: {
    zoomIn: {
      duration: 1500,
      easing: TWEEN.Easing.Sinusoidal.In,
    },
    numberOfReels: 5,
    numberOfSymbolsPerReel: [
      42, 38, 34, 30, 26,
    ],
    spin: {
      duration: 3000,
      speedFactor: 6,
      easing: TWEEN.Easing.Quadratic.InOut,
    },
    winlines: [{
      winningLineNumber: 0,
      positions: [0, 0, 0, 0, 0],
      winAmount: 100,
    }, {
      winningLineNumber: 1,
      positions: [1, 1, 1, 1, 1],
      winAmount: 100,
    }, {
      winningLineNumber: 2,
      positions: [2, 2, 2, 2, 2],
      winAmount: 100,
    }, {
      winningLineNumber: 3,
      positions: [0, 1, 1, 1, 1],
      winAmount: 50,
    }, {
      winningLineNumber: 4,
      positions: [0, 2, 2, 2, 2],
      winAmount: 50,
    }, {
      winningLineNumber: 5,
      positions: [1, 2, 2, 2, 2],
      winAmount: 50,
    }, {
      winningLineNumber: 6,
      positions: [1, 0, 0, 0, 0],
      winAmount: 25,
    }, {
      winningLineNumber: 7,
      positions: [1, 1, 1, 1, 1],
      winAmount: 25,
    }],
  },
};
