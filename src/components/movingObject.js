import TWEEN from '@tweenjs/tween.js';
import Component from '../sceneManager/component';

/**
 * This component applies movement on the X axis with a configurable duration.
 * @module MovingObject
 * @extends Component
 */
export default class MovingObject extends Component {
  /**
   * Starts the entity which creates a container with sprite from textureId
   * and immedialty calls {@link move} function which starts the movement.
   */
  start(entity) {
    this.entity = entity;
    this.move();
  }

  /**
   * Moves the entity on the X axis with a bit of extra random padding to create a better effect.
   * Duration is configurable in the scene.
   * TODO: add more confiurable parameters such as direction, X/Y lenghts, etc...
   */
  move() {
    new TWEEN.Tween(this.entity.container)
      .to({ x: window.innerWidth + Math.random() * 200, alpha: 0 }, this.entity.attributes.duration)
      .easing(TWEEN.Easing.Linear.None)
      .repeat(Infinity)
      .start();
  }
}
