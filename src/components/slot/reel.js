import * as PIXI from 'pixi.js';
import TWEEN from '@tweenjs/tween.js';

/**
 * Class representing a single reel. A slotmachine would usually create 3 or more
 * instances of the reel with different parameters and configurations.
 * @module Reel
 */
export default class Reel {
  constructor(numOfSymbols, reelParams) {
    this.container = new PIXI.Container();
    this.symbols = [];
    this.currentIndex = 0;
    this.container.pivot.x = reelParams.pivotOffset;
    this.container.pivot.y = reelParams.pivotOffset;
    this.numOfSymbols = numOfSymbols;
    this.reelParams = reelParams;
  }

  /**
   * Creates number of symbols on the reel which is limited by the parameter {@link numOfSymbols}
   */
  start() {
    for (let index = 0; index < this.numOfSymbols; index++) {
      this.createSymbols(this.reelParams.radius, index, this.numOfSymbols);
    }
  }

  /**
   * Creates number of symbols on the reel which is limited by the parameter {@link numOfSymbols}
   * @param {number} radius - Radius of the circle so we can position the elements properly.
   * @param {number} index - Index of in the loop so we can calculate the right angle.
   * @param {number} numOfElem - Number of total elements so we can calculate the right angle.
   */
  createSymbols(radius, index, numOfElem) {
    const symId = Math.round(Math.random() * 5);
    const sym = PIXI.Sprite.fromFrame(`ship${symId}`);
    sym.scale.x = 0.45;
    sym.scale.y = 0.45;
    sym.rotation = (index / (numOfElem / 2)) * Math.PI + 0.01;
    sym.x = (radius * Math.cos(sym.rotation)) + this.reelParams.pivotOffset;
    sym.y = (radius * Math.sin(sym.rotation)) + this.reelParams.pivotOffset;
    sym.symbolId = symId;
    this.container.addChild(sym);
    this.symbols.push(sym);
  }

  /**
   * Asynchronous spin function which rotates the wheel on stops on the right index.
   * TODO: make more parameters configurable, better caluclate the speed, rotation, etc...
   * @param {number} stopOnIndex - Index of the symbol where the reel stops.
   */
  async spin(stopOnIndex) {
    // calc: current rotation + few full circles of rotation
    const minimumRotation = this.container.rotation + ((Math.PI * (this.numOfSymbols % 4)) * this.reelParams.spin.speedFactor);
    // calc: full circle / all symbols * stopIndex
    const stopAngle = ((Math.PI * 2 / this.numOfSymbols) * stopOnIndex);
    // calc: half a circle (we want it to stop on the left) - stopAngle - correction
    // we correnct for leftover angle to always stop on the same place
    const endRotation = (Math.PI - stopAngle)
      - ((this.container.rotation) % (Math.PI * 2));

    return new Promise((resolve) => {
      this.spinningTween = new TWEEN.Tween(this.container)
        .to({ rotation: minimumRotation + endRotation }, this.reelParams.spin.duration)
        .easing(this.reelParams.spin.easing)
        .onComplete(() => {
          this.reelStopped(stopOnIndex);
          resolve();
        })
        .start();
    });
  }

  /**
   * Is called when reels are stopped with a symbol index where the reel has stopped.
   * @param {number} onIndex - Index of the symbol where the reel stops.
   */
  reelStopped(onIndex) {
    let symbolAboveId = onIndex - 1;
    let symbolBelowId = onIndex + 1;

    if (symbolAboveId < 0) {
      symbolAboveId = this.symbols.length - 1;
    }

    if (symbolBelowId > this.symbols.length - 1) {
      symbolBelowId = 0;
    }

    this.winningSymbols = [
      this.symbols[symbolAboveId],
      this.symbols[onIndex],
      this.symbols[symbolBelowId],
    ];
  }

  /**
   * @public
   * @member {array} winningSymbols - Array of winning symbol objects.
   */
  getWinningSymbols() {
    return this.winningSymbols;
  }

  /**
   * @public
   * @member {array} winningSymbolsIds - Array of winning symbol Ids. Useful for parsing results.
   */
  getWinningSymbolsIds() {
    const winningSymbolsIds = [];
    for (const winningSymbol of this.winningSymbols) {
      winningSymbolsIds.push(winningSymbol.symbolId);
    }
    return winningSymbolsIds;
  }
}
