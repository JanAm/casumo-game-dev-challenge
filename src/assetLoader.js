import * as PIXI from 'pixi.js';
import { Howl } from 'howler';

export default class AssetLoader {
  init() {
    this.loader = PIXI.loader;

    const assets = {
      spinButton: 'assets/fightJoy_01.png',
      ship0: 'assets/ship17.png',
      island: 'assets/island.png',
      ship1: 'assets/ship12.png',
      ship2: 'assets/ship13.png',
      ship3: 'assets/ship14.png',
      ship4: 'assets/ship15.png',
      ship5: 'assets/ship16.png',
      reelBackground: 'assets/reel-background.png',
      canonIsland: 'assets/canon-island.png',
      cloud: 'assets/smokeWhite4.png',
      textBackground: 'assets/balance.png',
    };

    // add all assets
    Object.keys(assets).forEach((key) => {
      this.loader.add(key, assets[key]);
    });

    this.loader.load();
    this.loader.onProgress.add(() => {
      // do progress bar here
    });
    this.loader.onError.add(() => {
      throw new Error('Something went wrong whilst loading resources :( ');
    });

    // resolve a promise so we can use this as async function
    return new Promise((resolve) => {
      this.loader.onComplete.add(resolve);
      AssetLoader.loadAudio();
    });
  }

  static loadAudio() {
    for (const key in AssetLoader.audioAssets) {
      if (Object.prototype.hasOwnProperty.call(AssetLoader.audioAssets, key)) {
        const sound = new Howl({
          src: [AssetLoader.audioAssets[key]],
        });
        AssetLoader.sounds[AssetLoader.audioAssets[key]] = sound;
      }
    }
  }
}

AssetLoader.audioAssets = {
  battleThemeA: 'assets/audio/battleThemeA.mp3',
  creek: 'assets/audio/creek.mp3',
  swoosh: 'assets/audio/swoosh.mp3',
};
AssetLoader.sounds = {};
